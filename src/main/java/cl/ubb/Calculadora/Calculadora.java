package cl.ubb.Calculadora;

public class Calculadora {
	
	public int sumar(int i, int j){
		
		int suma;
		
		suma = i + j;
		
		return suma;
		
	}

	public int multiplicar(int i, int j){
		int multiplicacion;
		
		multiplicacion = i * j;
		
		return multiplicacion;
	}
	
	public int dividir(int i, int j) throws DivisionException{
		
		if(j==0){
			throw new DivisionException();
		}else{
			return (i/j);
		}
	
	}
	
}
