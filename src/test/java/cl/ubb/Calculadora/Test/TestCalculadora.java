package cl.ubb.Calculadora.Test;

import static org.junit.Assert.*;

import org.junit.Test;

import cl.ubb.Calculadora.Calculadora;
import cl.ubb.Calculadora.DivisionException;

public class TestCalculadora {

	@Test
	public void SumarUnoMasUnoEsIgualDos() {
		int resultado;
		//arrange
		Calculadora calculadora = new Calculadora();
		
		//act
		resultado = calculadora.sumar(1,1);
		
		//assert
		assertEquals(2,resultado);
	}

	@Test
	public void SumarUnoPositivoMasDosNegativoEsIgualAMenosUno() {
		int resultado;
		//arrange
		Calculadora calculadora = new Calculadora();
		
		//act
		resultado = calculadora.sumar(1,-2);
		
		//assert
		assertEquals(-1,resultado);
	}
	
	@Test
	public void SumarUnoNegativoMasUnoNegativoEsMenosDos() {
		int resultado;
		//arrange
		Calculadora calculadora = new Calculadora();
		
		//act
		resultado = calculadora.sumar(-1,-1);
		
		//assert
		assertEquals(-2,resultado);
	}
	
	@Test
	public void MultiplicarDosPorTresEsIgualSeis() {
		int resultado;
		//arrange
		Calculadora calculadora = new Calculadora();
		
		//act
		resultado = calculadora.multiplicar(2,3);
		
		//assert
		assertEquals(6,resultado);
	}
	
	@Test
	public void MultiplicarMenosDosPorMenosTresEsIgualSeis() {
		int resultado;
		//arrange
		Calculadora calculadora = new Calculadora();
		
		//act
		resultado = calculadora.multiplicar(-2,-3);
		
		//assert
		assertEquals(6,resultado);
	}
	
	@Test
	public void DividirCuatroPorDosEsIgualDos() throws DivisionException {
		int resultado;
		//arrange
		Calculadora calculadora = new Calculadora();
		
		//act
		resultado = calculadora.dividir(4,2);
		
		//assert
		assertEquals(2,resultado);
	}
	
	@Test
	public void DividirSeisPorDosEsIgualTres() throws DivisionException {
		int resultado;
		//arrange
		Calculadora calculadora = new Calculadora();
		
		//act
		resultado = calculadora.dividir(6,2);
		
		//assert
		assertEquals(3,resultado);
	}
	
	@Test (expected=DivisionException.class)
	public void DividirCuatroPorCero() throws DivisionException{
		
		int resultado;
		//arrange
		Calculadora calculadora = new Calculadora();
		
		//act
		resultado = calculadora.dividir(4,0);
		
		//assert
		
	}
}
